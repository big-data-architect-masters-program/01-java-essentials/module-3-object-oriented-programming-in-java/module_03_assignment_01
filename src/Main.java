import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("1: Current Account");
        System.out.println("2: SB Account");
        System.out.print("Please select account type:");
        int at = scanner.nextInt();
        if (at == 2) {
            SBAccount sbAccount = new SBAccount();
            sbAccount.accountNumber = 1;
            System.out.println("Enter your name:");
            sbAccount.accountName = scanner.next();
            System.out.println("Enter Amount:");
            sbAccount.amount = scanner.nextDouble();
            sbAccount.show();
        } else {
            CurrentAccount currentAccount = new CurrentAccount();
            currentAccount.accountNumber = 1;
            System.out.println("Enter your name:");
            currentAccount.accountName = scanner.next();
            System.out.println("Enter Amount:");
            currentAccount.amount = scanner.nextDouble();
            currentAccount.show();
        }
    }
}

abstract class Account {
    int accountNumber;
    String accountName;
    Double amount;

    void show() {
        System.out.println("Account Number: " + accountNumber + " Account Name: " + accountName + " Amount: " + amount);
    }
}

class CurrentAccount extends Account {

}

class SBAccount extends Account {
    Double interest = 0.04;
}